FROM node:16.3.0

# GLOBAL INIT
COPY  . .

# RUN INSTALL DEPENDENCIES
RUN npm ci --quiet --no-optional --no-audit

# RUN BUILD CLIENT AND SERVER 
RUN npm run build

ENV NODE_ENV=prod
# start express server
CMD [ "npm", "start"]
